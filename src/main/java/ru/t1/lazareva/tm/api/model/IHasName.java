package ru.t1.lazareva.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}